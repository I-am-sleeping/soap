package by.training.soap.service;

import javax.jws.WebService;
import org.apache.cxf.binding.soap.SoapFault;
import net.webservicex.CurrencyConvertor;
import net.webservicex.CurrencyConvertorSoap;

@WebService(endpointInterface = "by.training.soap.service.CurrencyConverter",
            name = "currencyConverter")
public class CurrencyConverterImpl implements CurrencyConverter {

    @Override
    public double convertCurrency(double amount, Currency from, Currency to) {
        CurrencyConvertorSoap convertor = new CurrencyConvertor()
                .getCurrencyConvertorSoap();
        if (from == null || to == null) {
            throw new SoapFault("Invalid currency.",
                    SoapFault.FAULT_CODE_CLIENT);
        }
        double rate = convertor.conversionRate(
                net.webservicex.Currency.valueOf(from.toString()),
                net.webservicex.Currency.valueOf(to.toString()));
        return amount * rate;
    }

}
