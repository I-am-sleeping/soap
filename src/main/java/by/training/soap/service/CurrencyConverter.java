package by.training.soap.service;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService
public interface CurrencyConverter {
    @WebResult(name = "converted-value")
    double convertCurrency(@WebParam(name = "amount") double amount,
                           @WebParam(name = "from") Currency from,
                           @WebParam(name = "to") Currency to);
}
