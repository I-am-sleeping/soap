package by.training.soap.service;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "Currency")
@XmlEnum
public enum Currency {
    RUB, EUR, USD
}
